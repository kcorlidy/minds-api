import os
from pathlib import Path

from minds.profile import Profile as _Profile
import pkg_resources


class Profile(_Profile):
    config_dir = Path('/tmp/')


def test_init():
    args = 'username', 'password', {'cookie': True}, 'https://proxy.com'
    p = Profile(*args)
    assert p.username == args[0]
    assert p.password == args[1]
    assert p.cookie == args[2]
    assert p.proxy == args[3]
    assert p.dir


def test_from_config():
    args = 'username', 'password', {'cookie': True}, 'https://proxy.com'
    p_saved = Profile(*args)
    p_saved.save()
    p_loaded = Profile.from_config(args[0])
    assert p_loaded == p_saved


def test_save():
    args = 'username', 'password', {'cookie': True}, 'https://proxy.com'
    p = Profile(*args)
    p.save()
    assert os.path.exists(p.dir)
    with open(p.dir) as f:
        saved = f.read()
    expected = pkg_resources.resource_string('tests', 'fixtures/profile.toml').decode('utf8')
    assert saved == expected
